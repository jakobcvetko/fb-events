class AddStuffToEvent < ActiveRecord::Migration
  def change
    add_column :events, :ticket_uri, :string
    add_column :events, :pic_small, :string
    add_column :events, :location, :string
  end
end
