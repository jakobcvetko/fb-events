class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.integer :eid
      t.string :name
      t.integer :all_members_count
      t.integer :attending_count
      t.integer :declined_count
      t.integer :unsure_count
      t.integer :not_replied_count
      t.references :user

      t.timestamps
    end
  end
end
