class ChangeVenueType < ActiveRecord::Migration
  def self.up
   change_column :events, :venue, :text
  end

  def self.down
   change_column :events, :venue, :string
  end
end
