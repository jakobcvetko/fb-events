class CreateEventMembers < ActiveRecord::Migration
  def change
    create_table :event_members do |t|
      t.column :eid, :bigint
      t.column :uid, :bigint
      t.string :rsvp_status
      t.references :user

      t.timestamps
    end
  end
end
