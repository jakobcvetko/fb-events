class ChangeEidType < ActiveRecord::Migration
  def self.up
   change_column :events, :eid, :bigint
  end

  def self.down
   change_column :events, :eid, :integer
  end
end
