class ChangePictureColumn < ActiveRecord::Migration
  def change
    rename_column :events, :pic_small, :pic
  end
end
