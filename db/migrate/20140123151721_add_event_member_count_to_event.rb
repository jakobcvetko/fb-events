class AddEventMemberCountToEvent < ActiveRecord::Migration
  def change
    add_column :events, :rsvp_yes, :int
    add_column :events, :rsvp_maybe, :int
    add_column :events, :friends_rsvp, :int
    add_column :events, :all_rsvp, :int
  end
end
