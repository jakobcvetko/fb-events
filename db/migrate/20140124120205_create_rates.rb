class CreateRates < ActiveRecord::Migration
  def change
    create_table :rates do |t|
      t.references :user, index: true
      t.integer :rating
      t.integer :position
      t.references :event, index: true

      t.timestamps
    end
  end
end
