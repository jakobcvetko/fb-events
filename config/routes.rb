FbEvents::Application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  get 'auth/:provider/callback', to: 'sessions#create'
  get 'auth/failure', to: 'sessions#failure'
  get 'signout', to: 'sessions#destroy', as: 'signout'

  resources :events, only: [:index]
  get 'rate', controller: :events

  get 'sync_fb_event_members', controller: :background
  get 'sync_fb_events', controller: :background
  get 'count_event_members', controller: :background

  root to: 'events#index'
end
