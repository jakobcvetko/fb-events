ActiveAdmin.register_page "Dashboard" do

  #menu :priority => 1, :label => proc{ I18n.t("active_admin.dashboard") }

  content :title => proc{ I18n.t("active_admin.dashboard") } do

    columns do
      column do
        panel "Statistics" do
          table do
            tr do
              th "Users:"
              th "#{User.all.count} users"
            end
            tr do
              th "Per user average rates:"
              th "#{Rate.average_anwsers_by_user.round(2)} rates"
            end
            tr do
              th "Anwsered rates:"
              th "#{Rate.all.count} / #{User.all.count * 30} possible"
            end
            tr do
              th "Average rating (1-5):"
              th "#{Rate.average(:rating).round(2)} stars"
            end
          end
        end
      end

      column do
        panel "Info" do
          para "Welcome to ActiveAdmin."
        end
      end
    end
  end # content
end
