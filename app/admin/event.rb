ActiveAdmin.register Event do

  menu false
  scope :all

  User.all.each do |u|
    scope u.name, "scope_for_user_#{u.id}".to_sym
  end

  index do
    column :name, sortable: true
    column :all_members_count, sortable: true
    column :attending_count, sortable: true
    column :declined_count, sortable: true
    column :unsure_count, sortable: true
    column :not_replied_count, sortable: true
    column :location, sortable: true
    column :start_time
    column :end_time
    column :privacy
    column :description do |e|
      e.description.truncate(50) rescue ''
    end
    column :venue
  end

end
