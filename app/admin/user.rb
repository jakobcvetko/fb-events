ActiveAdmin.register User do
  actions :index

  index do
    column :id
    column :name
    column :uid
    column :oauth_token do |u|
      "<div style='word-wrap:break-word; width: 600px'>#{u.oauth_token}</div>".html_safe
    end
    column :created_at do |u|
      time_ago_in_words u.created_at
    end
  end
end
