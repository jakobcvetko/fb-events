class BackgroundController < ApplicationController

  # def sync
  #   user = User.find_by_id(params[:user_id])
  #   if user.present?
  #     EventMember.sync_fb_events(user)
  #     Event.sync_fb_events(user)
  #     Event.count_event_members(user)
  #     render text: "OK"
  #   else
  #     render text: "Fail"
  #   end
  # end

  def sync_fb_event_members
    user = User.find_by_id(params[:user_id])
    if user.present?
      EventMember.sync_fb_events(user)
      render text: "OK"
    else
      render text: "Fail"
    end
  end

  def sync_fb_events
    user = User.find_by_id(params[:user_id])
    if user.present?
      Event.sync_fb_events(user)
      render text: "OK"
    else
      render text: "Fail"
    end
  end

  def count_event_members
    user = User.find_by_id(params[:user_id])
    if user.present?
      Event.count_event_members(user)
      render text: "OK"
    else
      render text: "Fail"
    end
  end

end
