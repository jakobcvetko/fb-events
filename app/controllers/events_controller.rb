class EventsController < ApplicationController

  expose(:events) do
    if current_user.nil?
      []
    else
      current_user.events.sort_by{|e| - e.ratio}.first(30)
    end
  end

  def index
  end

  def order
    @order ||= session[:order]
  end

  def switch_order sort
    if @order["DESC"].present?
      @order = sort
    else
      @order = sort + " DESC"
    end
  end

  def rate
    rating = params[:rating]
    eid = params[:eid]
    position = params[:position]

    rate = Rate.where(event_id: eid, user_id: current_user.id).first_or_initialize

    rate.position = position
    rate.rating = rating
    rate.save!

    flash[:notice] = "Rate saved!"
    ancor = ""

    tmp = position.to_i - 4
    if tmp > 0
      ancor = "pos-#{tmp}"
    end

    redirect_to events_path(anchor: ancor)
  end
end
