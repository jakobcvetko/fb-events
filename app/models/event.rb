class Event < ActiveRecord::Base

  User.all.each do |u|
    define_singleton_method ["scope","for","user",u.id].join("_") do
      u.events
    end
  end

  belongs_to :user
  validates_presence_of :user

  def self.scopes
    User.all.map &:name.to_sym
  end

  def attendence_ratio
    all_rsvp.to_f/all_members_count.to_f rescue 0
  end

  def local_attendence_ratio
    friends_rsvp.to_f/all_rsvp.to_f rescue 0
  end

  def ratio
    # local_attendence_ratio + (attendence_ratio * 0.01)
    friends_rsvp or 0
  end

  def facebook_url
    "https://www.facebook.com/events/#{eid}"
  end

  def rated user
    @tmp_rate ||= Rate.where(event_id: id, user_id: user.id).first.rating rescue 0
  end

  def rated? n, user
    r = rated(user)
    n <= r
  end

  def self.count_event_members user
    user.events.each do |e|
      puts "Counting event members"
      #e.rsvp_yes = user.event_members.where(eid: e.eid, rsvp_status: 'attending').count
      #e.rsvp_maybe = user.event_members.where(eid: e.eid, rsvp_status: 'unsure').count
      e.friends_rsvp = user.event_members.where(eid: e.eid).count
      e.all_rsvp = e.attending_count + e.unsure_count
      e.save!
    end
  end

  def self.sync_fb_events user

    fql = "SELECT eid,name,attending_count,all_members_count,declined_count,not_replied_count,unsure_count
            FROM event
            WHERE eid IN(
              SELECT eid FROM event_member
              WHERE uid IN ( SELECT uid2 FROM friend WHERE uid1=me() )
              AND (rsvp_status='attending' OR rsvp_status='unsure')
            )"

    FbGraph::Query.new(fql).fetch(access_token: user.oauth_token).each do |fb_event|
      puts "Syncing event"

      event = Event.where(eid: fb_event[:eid]).first_or_initialize

      event.name = fb_event[:name]
      event.all_members_count = fb_event[:all_members_count]
      event.attending_count = fb_event[:attending_count]
      event.declined_count = fb_event[:declined_count]
      event.unsure_count = fb_event[:unsure_count]
      event.not_replied_count = fb_event[:not_replied_count]
      event.user = user

      event.save!
    end

    fql = "SELECT eid,location,start_time,end_time,pic,description,venue
            FROM event
            WHERE eid IN(
              SELECT eid FROM event_member
              WHERE uid IN ( SELECT uid2 FROM friend WHERE uid1=me() )
              AND (rsvp_status='attending' OR rsvp_status='unsure')
            )"

    FbGraph::Query.new(fql).fetch(access_token: user.oauth_token).each do |fb_event|

      event = Event.where(eid: fb_event[:eid]).first_or_initialize

      event.location = fb_event[:location]
      event.start_time = fb_event[:start_time]
      event.end_time = fb_event[:end_time]
      event.pic = fb_event[:pic]
      event.description = fb_event[:description]
      event.venue = fb_event[:venue].to_s
      event.user = user

      event.save!
    end


  end

end

