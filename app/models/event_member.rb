class EventMember < ActiveRecord::Base

  belongs_to :user

  def self.sync_fb_events user

    fql = "SELECT eid,uid,rsvp_status FROM event_member
            WHERE uid IN ( SELECT uid2 FROM friend WHERE uid1=me() )
            AND (rsvp_status='attending' OR rsvp_status='unsure')"

    FbGraph::Query.new(fql).fetch(access_token: user.oauth_token).each do |fb_event|
      puts "Syncing member"
      event_member = EventMember.where(eid: fb_event[:eid], uid: fb_event[:uid]).first_or_initialize

      event_member.eid = fb_event[:eid]
      event_member.uid = fb_event[:uid]
      event_member.rsvp_status = fb_event[:rsvp_status]
      event_member.user = user

      event_member.save!
    end

  end
end
