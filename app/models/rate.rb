class Rate < ActiveRecord::Base
  belongs_to :user
  belongs_to :event

  def self.average_anwsers_by_user
    responses_array = Rate.distinct.count(group: :user_id).map{|r| r[1]}
    responses_array.sum / User.all.count.to_f
  end
end
